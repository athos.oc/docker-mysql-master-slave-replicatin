#!/bin/bash
sleep 20

position=$(mysql --host $MASTER_HOST_SERVICE_NAME -u$MASTER_HOST_USER -p$MASTER_HOST_PASSWORD -e 'show master status \G' | grep Position | sed -n -e 's/^.*: //p')
file=$(mysql --host $MASTER_HOST_SERVICE_NAME -u$MASTER_HOST_USER -p$MASTER_HOST_PASSWORD -e 'show master status \G'     | grep File     | sed -n -e 's/^.*: //p')

mysql --host localhost -uroot -proot -e 'stop slave;'
mysql --host localhost -uroot -proot -e "CHANGE MASTER TO MASTER_HOST = 'sqlmaster', MASTER_USER = 'root', MASTER_PASSWORD = '$MYSQL_ROOT_PASSWORD', MASTER_PORT = 3306, MASTER_LOG_FILE = '$file', MASTER_LOG_POS = $position"
mysql --host localhost -uroot -proot -e 'start slave;'
